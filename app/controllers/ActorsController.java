package controllers;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static akka.pattern.Patterns.ask;
import actors.HelloActorProtocol.SayHello;
import scala.concurrent.duration.Duration;

/**
 * Created by caiolins on 7/18/16.
 */
@Singleton
public class ActorsController extends Controller {

    private final ActorRef helloActor;
    private final ActorRef rudeActor;
    private final ActorRef niceActor;

    @Inject
    public ActorsController(ActorSystem system,
                            @Named("hello-actor") ActorRef helloActor,
                            @Named("nice-actor") ActorRef niceActor,
                            @Named("rude-actor") ActorRef rudeActor) {
        this.helloActor = helloActor;
        this.niceActor = niceActor;
        this.rudeActor = rudeActor;
        system.scheduler().scheduleOnce(
                Duration.create(5000, TimeUnit.MILLISECONDS),
                () -> rudeActor.tell("hi!", rudeActor),
                system.dispatcher()
        );
    }

    public Result sayHello(String name) {
        //CompletionStage<Result> nice = FutureConverters.toJava(ask(niceActor, new SayHello(name), 1000)).thenApply(response -> ok ((String) response));
        return ok("hmmmm");
    }
}
