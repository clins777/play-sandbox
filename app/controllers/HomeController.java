package controllers;

import actors.ConfiguredActor;
import actors.HelloActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Animal;
import org.h2.tools.SimpleResultSet;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.mvc.*;
import scala.compat.java8.FutureConverters;
import services.AnimalManager;
import services.AnimalManagerApi;
import services.Crawler;
import views.html.*;
import views.html.coursera.javascript;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;
import java.util.Map;

@Singleton
public class HomeController extends Controller {

    private WSClient ws;
    private Crawler crawler;
    private SimpleResultSet srs;

    @Inject
    private AnimalManager manager;

    @Inject
    private AnimalManagerApi managerApi;

    @Inject
    public HomeController(WSClient ws, Crawler crawler,
                          SimpleResultSet srs) {
        this.ws = ws;
        this.crawler = crawler;
        this.srs = srs;
    }

    @Transactional
    public Result index() {
        return ok(index.render("Main"));
    }

    @Transactional
    public Result viewAnimals() {
        return ok(animals.render(manager.getAnimals()));
    }

    public Result addAnimalForm() { return ok(animal_add.render()); }

    @Transactional
    public Result addAnimal() {
        Map<String, String[]> form = request().body().asFormUrlEncoded();
        Integer id = null;
        String name = form.get("name")[0];
        String type = form.get("type")[0];
        String personality = form.get("personality")[0];
        Logger.debug("form data = " + name + " " + type + " " + personality);
        Animal animal = new Animal(id, name, type, personality);
        manager.addAnimal(animal);
        return ok(animals.render(manager.getAnimals()));
    }

    @Transactional
    public Result editAnimalForm(Integer id) {
        Animal existing = manager.getAnimal(id);
        if(existing == null)
            return badRequest("error opening form to edit animal, not found in database");
        return ok(animal_edit.render(existing));

    }

    @Transactional
    public Result editAnimal(Integer id) {
        Map<String, String[]> form = request().body().asFormUrlEncoded();
        String name = form.get("name")[0];
        String type = form.get("type")[0];
        String personality = form.get("personality")[0];
        Logger.debug("dados do form = " + name + " " + type + " " + personality);
        Animal animal = new Animal(id, name, type, personality);
        if(manager.editAnimal(animal) == null)
            return badRequest("error editing animal, not found in database");
        return redirect(routes.HomeController.viewAnimals());
    }

    @Transactional
    public Result deleteAnimal(Integer id) {
        if(manager.deleteAnimal(id)) {
            return ok("deleted");
        } else {
            return badRequest("delete failed");
        }
    }

    @Transactional
    public Result getAnimalsJs() {
        List<Animal> animals = manager.getAnimals();
        if(animals.size() <= 0)
            return badRequest("no animals in database");
        return ok(Json.toJson(animals));
    }

    @Transactional
    public Result getAnimalJs(Integer id) {
        Animal animal = manager.getAnimal(id);
        if(animal == null)
            return badRequest("no animal with that ID");
        return ok(Json.toJson(animal));
    }

    @Transactional
    public Result addAnimalJs() {
        if(managerApi.addAnimalJs(request().body().asJson()))
            return created();
        return badRequest("incorrect format, try again");
    }

    // curl -v -X POST -H'Content-Type: application/json' -d'{"hey": "ho"}' localhost:9000/api/animals/add

    public Result jsExercise() {return ok(javascript.render()); }
}
