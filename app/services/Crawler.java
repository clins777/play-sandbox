package services;

import com.fasterxml.jackson.databind.JsonNode;
import org.h2.tools.Csv;
import org.h2.tools.SimpleResultSet;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import play.Logger;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletionStage;

/**
 * Created by caiolins on 7/1/16.
 */
public class Crawler {

    private final String[] states = {
            "AC", "AP", "AM", "Bahia", "CE", "ES", "GO",
            "MA", "MG", "PA", "PB", "PR", "PE", "PI", "RN",
            "RS", "RJ", "RO", "SC", "SP", "SE"};

    private WSClient wsClient;

    private SimpleResultSet srs;

    private int stateCount;

    @Inject
    public Crawler(WSClient ws) {
        this.wsClient = ws;
        this.stateCount = 0;
    }

    private void saveCsv(String path, SimpleResultSet srs)
    {
        try {
            Csv csv = new Csv();
            csv.write(path, srs, null);
            Logger.debug("SAVED");
            logCsv(path);
        } catch(Exception e) {
            Logger.debug("ERROR");
        }
    }

    public void getStoresFromPage(Document doc, SimpleResultSet srs)
    {
        Elements allStores = doc.getElementsByClass("retailer-result");
        Iterator<Element> storeIterator = allStores.iterator();

        while(storeIterator.hasNext())
        {
            Element store = storeIterator.next();
            Elements data = store.children();
            Iterator<Element> dataIterator = data.iterator();

            String csvData[] = new String[10];

            while(dataIterator.hasNext())
            {
                Element bundle = dataIterator.next();

                switch (bundle.className())
                {
                    case "result-title": // NAME
                        csvData[0] = bundle.getElementsByClass("name-title").iterator().next().ownText();
                        break;
                    case "address": // ADDRESS, DISTRICT, STATE, ZIP_CODE, COUNTRY
                        Elements subBundle = bundle.getElementsByAttribute("itemprop");
                        Iterator<Element> bundleIterator = subBundle.iterator();

                        for(int i = 1; i < 6; i++)
                            csvData[i] = bundleIterator.next().ownText();

                        break;
                    case "phone": // PHONE
                        csvData[6] = bundle.getElementsByClass("click-to-call").iterator().next().ownText();
                        break;
                    case "get-direction": // LINK ??
                        //sb.append("\n\tDIRECTIONS: ");
                        //sb.append(bundle.getElementsByAttribute("href").iterator().next().attr("href"));
                        break;
                    case "geo-cords": // LATITUDE, LONGITUDE, WEBSITE
                        Elements locationBundle = bundle.getElementsByAttribute("itemprop");
                        Iterator<Element> locationBundleIterator = locationBundle.iterator();

                        for(int i = 7; i < 10; i++)
                            csvData[i] = locationBundleIterator.next().attr("content");
                        break;
                    default:
                        break;
                }
            }
            srs.addRow(csvData[0], csvData[1], csvData[2], csvData[3], csvData[4], csvData[5], csvData[6], csvData[7], csvData[8], csvData[9]);

        }
    }

    public void requestPages(String url, int page, SimpleResultSet srs)
    {
        // Metodo recursivo que cria um novo request a cada iteracao
        WSRequest request = wsClient.url(url + page);
        request.get()
                .thenApply(response -> {

                    Document doc = Jsoup.parse(response.getBody());
                    Elements stores = doc.getElementsByClass("retailer-result");

                    if(stores.size() > 0)
                    {
                        getStoresFromPage(doc, srs);
                        Logger.debug("\tLoja " + page);
                        requestPages(url, page + 1, srs);
                    } else
                    {
                        stateCount++;
                        Logger.debug("STATE DONE! STATE COUNT: " + stateCount);
                    }

                    return null;
                });
    }

    public void multipleSynchronousRequests(int stateIndex, SimpleResultSet srs) {

        if(stateIndex < states.length)
        {
            WSRequest request = wsClient.url("https://www.heliar.com.br/pt-br/onde-comprar?state=" + states[stateIndex] + "&city=&region=&dealer_type=1&submit=");
            CompletionStage<WSResponse> cs = request.get();
                cs.thenApply(response -> {

                    Document doc = Jsoup.parse(response.getBody());

                    Elements allStates = doc.getElementById("state").getElementsByTag("option");
                    Iterator<Element> stateIterator = allStates.iterator();

                    String stateName = "NULL";

                    while(stateIterator.hasNext()) {
                        Element state = stateIterator.next();
                        if(state.attr("selected").equalsIgnoreCase("selected")) {
                            stateName = state.attr("value");
                        }
                    }

                    Logger.debug("Crawling state: " + stateName);

                    getStoresFromPage(doc, srs);

                    //Logger.debug("\tLoja 1");

                    String pageRequestUrl = "https://www.heliar.com.br/pt-br/onde-comprar?state=" + stateName + "&city=&region=&dealer_type=1&submit=&page=";

                    requestPages(pageRequestUrl, 2, srs);

                    multipleSynchronousRequests(stateIndex + 1, srs);

                    return null;
                });

        } else
        {
            Timer timer = new Timer();
            timer.schedule( new TimerTask()
            {
                public void run() {
                    if(stateCount < states.length)
                    {
                        Logger.debug("AINDA NAO");
                    } else
                    {
                        String path = "lojas.csv";
                        saveCsv(path, srs);
                        timer.cancel();
                    }

                }
            }, 0, 3000);
        }
    }

    private void checkIfOver() {
        try {

        } catch (Exception e)
        {
            Logger.debug("wow");
        }
    }

    public void logCsv(String csvPath) {
        try {
            Logger.debug("### PRINTING CSV START ###");
            ResultSet rs = new Csv().read(csvPath, null, null);
            ResultSetMetaData meta = rs.getMetaData();
            while (rs.next()) {
                for (int i = 0; i < meta.getColumnCount(); i++) {
                    Logger.debug(meta.getColumnLabel(i + 1) + ": " + rs.getString(i + 1));
                }
            }
            rs.close();
            Logger.debug("### PRINTING CSV END ###");
        } catch (Exception e) {
            Logger.debug("erro salvando csv");
        }
    }

    public void crawl()
    {
        Logger.debug("Number of states: " + states.length);
        this.srs = new SimpleResultSet();
        this.srs.addColumn("NAME", Types.VARCHAR, 255, 0);
        this.srs.addColumn("ADDRESS", Types.VARCHAR, 255, 0);
        this.srs.addColumn("DISTRICT", Types.VARCHAR, 255, 0);
        this.srs.addColumn("STATE", Types.VARCHAR, 255, 0);
        this.srs.addColumn("ZIP_CODE", Types.VARCHAR, 255, 0);
        this.srs.addColumn("COUNTRY", Types.VARCHAR, 255, 0);
        this.srs.addColumn("PHONE", Types.VARCHAR, 255, 0);
        this.srs.addColumn("LATITUDE", Types.VARCHAR, 255, 0);
        this.srs.addColumn("LONGITUDE", Types.VARCHAR, 255, 0);
        this.srs.addColumn("WEBSITE", Types.VARCHAR, 255, 0);
        multipleSynchronousRequests(0, this.srs);
    }

}
