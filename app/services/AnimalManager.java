package services;

import models.Animal;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by caiolins on 7/12/16.
 */
public class AnimalManager {

    private JPAApi jpaApi;

    @Inject
    public AnimalManager(JPAApi api) {
        this.jpaApi = api;
    }

    public Animal addAnimal(Animal animal) {
        jpaApi.em().persist(animal);
        return animal;
    }

    public boolean exists(Integer id) { return (jpaApi.em().find(Animal.class, id) != null); }

    public Animal getAnimal(Integer id) {
        Animal animal = jpaApi.em().find(Animal.class, id);
        if(exists(id))
            return animal;
        return null;
    }

    public List<Animal> getAnimals() {
        Query query = jpaApi.em().createQuery("SELECT a FROM Animal a");
        List<Animal> animals = query.getResultList();
        return animals;
    }

    public boolean deleteAnimal(Integer id) {
        Animal animal = getAnimal(id);
        if(animal != null) {
            jpaApi.em().remove(animal);
            return true;
        }
        return false;
    }

    public Animal editAnimal(Animal animal) {
        Animal existing = getAnimal(animal.getId());
        if(existing != null) {
            existing.setName(animal.getName());
            existing.setType(animal.getType());
            existing.setPersonality(animal.getPersonality());
            return existing;
        }
        return null;
    }
}
