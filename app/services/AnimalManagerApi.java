package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.Animal;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;

import javax.inject.Inject;

/**
 * Created by caiolins on 7/15/16.
 */
public class AnimalManagerApi {

    private JPAApi jpaApi;

    @Inject
    public AnimalManagerApi(JPAApi api) {
        this.jpaApi = api;
    }

    @Transactional
    public boolean addAnimalJs(JsonNode jn) {
        if(jn.has("name") && jn.has("type") && jn.has("personality") && !jn.has(3)) {
            Animal animal = new Animal(null, jn.get("name").asText(), jn.get("type").asText(), jn.get("personality").asText());
            jpaApi.em().persist(animal);
            return true;
        }
        return false;
    }
}
