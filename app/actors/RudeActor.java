package actors;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;
import play.Logger;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.concurrent.TimeUnit;

/**
 * Created by caiolins on 7/18/16.
 */
public class RudeActor extends UntypedActor {

    public static Props props = Props.create(RudeActor.class).withRouter(new RoundRobinPool(4));

    @Inject
    private ActorSystem system;

    @Inject
    @Named("nice-actor")
    private ActorRef niceActor;

    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof String) {
            Logger.debug("I am rude and I am reading the message: " + message);
            system.scheduler().scheduleOnce(
                    Duration.create(3000, TimeUnit.MILLISECONDS),
                    () -> {
                        Logger.debug("What an ugly message! - sends to nice actor -");
                        niceActor.tell(message, niceActor);
                    }, system.dispatcher()
            );
            //sender().tell("Hello, " + ((BeRude) message).name, self());
        }
    }
}
