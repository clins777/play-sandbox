package actors;

/**
 * Created by caiolins on 7/18/16.
 */
public class NiceActorProtocol {

    public static class BeNice {

        public final String name;

        public BeNice(String name) {
            this.name = name;
        }
    }
}