package actors;

import akka.actor.UntypedActor;
import play.Configuration;

import javax.inject.Inject;

/**
 * Created by caiolins on 7/15/16.
 */
public class ConfiguredActor extends UntypedActor {

    private Configuration configuration;

    @Inject
    public ConfiguredActor(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof HelloActorProtocol.SayHello) {
            sender().tell(configuration.getString("my.config"), self());
        }
    }
}
