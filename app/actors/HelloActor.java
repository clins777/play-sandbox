package actors;

import akka.actor.Props;
import akka.actor.UntypedActor;
import actors.HelloActorProtocol.SayHello;

public class HelloActor extends UntypedActor {

    public static Props props = Props.create(HelloActor.class);

    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof SayHello) {
            sender().tell("Hello, " + ((SayHello) message).name, self());
        }
    }
}
