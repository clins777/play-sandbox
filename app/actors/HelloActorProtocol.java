package actors;

/**
 * Created by caiolins on 7/15/16.
 */
public class HelloActorProtocol {

    public static class SayHello {

        public final String name;

        public SayHello(String name) {
            this.name = name;
        }
    }
}
