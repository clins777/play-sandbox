package actors;

/**
 * Created by caiolins on 7/18/16.
 */
public class RudeActorProtocol {

    public static class BeRude {

        public final String name;

        public BeRude(String name) {
            this.name = name;
        }
    }
}
