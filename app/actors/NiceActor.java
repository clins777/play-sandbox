package actors;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import actors.NiceActorProtocol.BeNice;
import actors.HelloActorProtocol.SayHello;
import play.Logger;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.concurrent.TimeUnit;

/**
 * Created by caiolins on 7/18/16.
 */
public class NiceActor extends UntypedActor {

    public static Props props = Props.create(NiceActor.class);

    @Inject
    private ActorSystem system;

    @Inject
    @Named("rude-actor")
    private ActorRef rudeActor;

    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof String) {
            Logger.debug("I am nice and I am reading the message: " + message);
            system.scheduler().scheduleOnce(
                    Duration.create(3000, TimeUnit.MILLISECONDS),
                    () -> {
                        Logger.debug("What a beautiful message! - sends to rude actor -");
                        rudeActor.tell(message, rudeActor);
                    }, system.dispatcher()
            );
        }
    }
}
