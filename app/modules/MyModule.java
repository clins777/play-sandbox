package modules;

import actors.HelloActor;
import actors.NiceActor;
import actors.RudeActor;
import com.google.inject.AbstractModule;
import play.libs.akka.AkkaGuiceSupport;

/**
 * Created by caiolins on 7/15/16.
 */
public class MyModule extends AbstractModule implements AkkaGuiceSupport {

    @Override
    protected void configure() {
        bindActor(HelloActor.class, "hello-actor");
        bindActor(NiceActor.class, "nice-actor");
        bindActor(RudeActor.class, "rude-actor");
    }
}
